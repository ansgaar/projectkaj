/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

/**
 *
 * @author David
 */
public class Account {
    private String username;
    private String password;
    
    /**
     *
     */
  
    public Account(String username, String password) {
        this.username = username;
        this.password = password;
    }
    
      public Account(){
        this("","");
    };
    
    
    public void update(Account account){
        this.username = account.username;
        this.password = account.password;
    }
    
    public Account getCopy(){
        Account copyAccount = new Account();
        copyAccount.username = this.username;
        copyAccount.password = this.password;
        return copyAccount;
    }
    
    public void setUsername(String username){
        this.username = username;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getUsername() {
        return this.username;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public String toString() {
        return "Username: " + this.username + "Password: " + this.password;
    }
}
