/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

/**
 *
 * @author David
 */
@ManagedBean(name="accountBean")
@Named
@SessionScoped
public class AccountBean implements Serializable{
    
    @Inject
    private Service service;
    private Account account;
    private Account selectedAccount;
    private Account validAccount;
    
    @PostConstruct
    public void init() {
    account = new Account();
}
    
    public Account getAccount() {
        if(account==null){
            account= new Account();
        }
        return account;
    }
    
    public boolean isSelected() {
        return selectedAccount != null;
    }
    
    public String newAccount() {
        selectedAccount = null;
        account = new Account();
        return "accountDetail";
    }
    
    public String selectAccount(Account selected) {
        this.selectedAccount = selected;
        account = selected.getCopy();
        return "accountDetail";
    }
    
     public String createAccount() {
        service.addAccount(account);
        return toAccountList();
    }

    public String updateAccount() {
        selectedAccount.update(account);
        return toAccountList();
    }

    public String deleteAccount() {
        service.removeAccount(selectedAccount);
        return toAccountList();
    }

    public List<Account> getPersons() {
        return service.getAccounts();
    }
    
    public String toAccountList() {
        selectedAccount = null;
        return "personList";
    }
    
    public String login() {
		validAccount = service.getValidAccount(account);
		if (validAccount != null) {
			account.update(validAccount);
                        validAccount = null;
			return "companiesOverviewPage";
		} else {
			account = new Account();
			return "index";
		}
	}
}
