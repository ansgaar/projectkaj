/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Account;

import Company.Company;
import java.util.List;
import java.util.ArrayList;
import javax.inject.Named;
import javax.faces.bean.ApplicationScoped;

@Named(value="service")
@ApplicationScoped
public class Service {
    private List<Account> accounts = new ArrayList<Account>();
        
    
    public Service() {
        accounts.add(new Account("David", "whatever"));
    }

    public List<Account> getAccounts() {
        return accounts;
    }
    
    public void addAccount(Account account) {
        accounts.add(account);
    }
    
    public void removeAccount(Account account) {
        accounts.remove(account);
    }
    
    public Account getValidAccount(Account account) {
      //  System.out.println("This is the account we are checking" +account.toString());
		for (Account u : accounts) {
			if (u.getUsername().equals(account.getUsername())
					&& u.getPassword().equals(account.getPassword())) {
				return u;
			}
		}
		return null;
	}

    public void removeCompany(Company selectedCompany) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Company> getCompanies() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void addCompany(Company company) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

