/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Company;

import Account.Service;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Alice
 */

    @Named
@SessionScoped
public class CompanyBean implements Serializable{
    
    @Inject
    private Service service;
    private Company company;
    private Company selectedCompany;
  //  private Account validAccount;
    
    public Company getCompany() {
        return company;
    }
    
    public boolean isSelected() {
        return selectedCompany != null;
    }
    
    public String newAccount() {
        selectedCompany = null;
        company = new Company();
        return "accountDetail";
    }
    
    public String selectCompany(Company selected) {
        this.selectedCompany = selected;
        company = selected.getCopy();
        return "accountDetail";
    }
    
     public String createAccount() {
        service.addCompany(company);
        return toAccountList();
    }

    public String updateAccount() {
        selectedCompany.update(company);
        return toAccountList();
    }

    public String deleteCompany() {
        service.removeCompany(selectedCompany);
        return toCompanyList();
    }

    public List<Company> getPersons() {
        return service.getCompanies();
    }
    
    public String toAccountList() {
        selectedCompany = null;
        return "personList";
    }
    
       private String toCompanyList() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    } 
}
