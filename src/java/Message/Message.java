/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Message;

import Account.Account;
import Company.Company;
import java.util.Date;

/**
 *
 * @author Alice
 */
public class Message {
    Company company;
    Account account;
    Date date;
    
    public Message(){};
    public void setCompany(Company company) {
        this.company = company;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Company getCompany() {
        return company;
    }

    public Account getAccount() {
        return account;
    }

    public Date getDate() {
        return date;
    }
    
}
